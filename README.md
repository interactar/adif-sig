# Adif-Sig

## Instalación de nodejs
https://github.com/nodesource/distributions
según la distribución, la url que utilizas.

esta aplicación corre ok con la 0.10 y 0.12

también necesita npm (maneja los paquetes de node, como apt / yum en los sistemas operativos.)

## Instalación de aplicación
1-Clone de git (bitbucket)
git clone git@bitbucket.org:interactar/adif-sig.git

2-Instalación de dependencias
cd adif-sig && npm install

3-Lanzar aplicación:
node index.js

## Configuracion

### Frontend
La `app` declara `GEOSERVER_URL` en 3 lugares:
```
  public/script/app/app.js
  public/script/app/app2.js
  public/script/app/sources.js
```
En los 3 casos se hace al principio del archivo y es la variable que usan los sources y el print plugin. Reemplazar.

### Backend
La aplicacion usa archivos de configuracion segun el NODE_ENV que se use. Por defecto usa el archivo `config/default.json`

#### Geoserver URL
En la *key* `geoserver` hay que proveer la URL donde se encuentra corriendo la instancia de geoserver:
```
  "geoserver": {
    "host": "geoserver.staging.adif.interactar.com:80"
  }
```
Reemplazar `host` con la URL que corresponda. El puerto es opcional.

#### DB

En la *key* `postgresql` hay que suministrar la configuracion y credenciales de conexion a PostGIS:
```
  "postgresql": {
    "protocol": "postgres",
    "slashes": true,
    "hostname": "localhost",
    "port": "5432",
    "auth": "USER:PASS",
    "dbname": "adif",
    "pathname": "adif"
  }
```
Cambiar *USER:PASS*

## Automatización (daemonizacion) start/stop  
si es upstart.
cp misc/etc/init/adif-web.conf /etc/init/adif-web.conf
modificar en /etc/init/adif-web.conf el path de forma tal que la aplicación corra donde debe.
export home='/home/webon/staging/web'

luego probar restart.

###TODO: Agregar systemd

## Opcional
Recomendado Utilizar con nginx, para que haga de reverse proxy y toda la aplicación funcione bajo los mismos ports.

1- instalar nginx, apt-get / yum install nginx
2- cp misc/etc/nginx/sites-available/default /etc/nginx/sites-available/default
3- ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
4- service nginx restart

`mumble mumble`
