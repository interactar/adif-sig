SELECT gid, nombre, tipo, jurisdiccion
FROM jurisdicciones
WHERE tipo = 'Departamento' OR tipo = 'Provincia'
ORDER BY tipo DESC, jurisdiccion, nombre;
