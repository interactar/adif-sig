var
  debug = require("debug")("adif:server"),
  express = require("express"),
  // compression = require("compression"),
  config = require("config"),
  adif = require("./lib/adif"),
  frontend = require("./lib/frontend"),
  edit = require("./lib/edit"),
  vistas = require("./lib/vistas"),
  proxy = require("./lib/proxy"),
  app = express();



// app.use(compression());

adif(app);
app.use(config.get('proxy').endpoint, proxy());

app.use("/edit.html", edit());
app.use("/script/app/layers.js", vistas("/script/app/layers.js"));
app.use("/script/app/mapparams.js", vistas("/script/app/mapparams.js"));
app.use("/script/app/tree_groups.js", vistas("/script/app/tree_groups.js"));
app.use("/", frontend());

app.use('/wfsbufferselect', require('./lib/wfsbuffer')());
app.use('/wfspolygonselect', require('./lib/wfspolygon')());
// app.use('/wfsjurisdiccion', require('./lib/wfsjurisdiccionselect')(app));
app.use('/jurisdicciones', require('./lib/jurisdicciones')(app));


app.listen(process.env.PORT || 3000);
debug("Listening on port %s", process.env.PORT);
