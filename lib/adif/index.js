var debug = require("debug")("adif:main");
var massive = require('massive');
var url = require('url');
var config = require("config");

module.exports = adif;

function adif(app) {
  var dbparams = config.get('postgresql');
  var db = massive.connectSync({connectionString: url.format(dbparams)});
  debug('Connection to DB done');
  app.set('db',db);

  app.on("error", function(err) {
    console.error("ADIF Error triggered: %s", err.stack.toString());
  });

}
