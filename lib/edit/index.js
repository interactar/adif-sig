var basicAuth = require('basic-auth');

var auth = function (req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.sendStatus(401);
  };

  var user = basicAuth(req);

  if (!user || !user.name || !user.pass) {
    return unauthorized(res);
  };

  if (user.name === 'foo' && user.pass === 'bar') {
    return next();
  } else {
    return unauthorized(res);
  };
};

var config = require("config"),
  express = require("express"),
  resolve = require("path").resolve;

module.exports = function() {
  var router = express.Router();

  router.use(auth, express.static(resolve(__dirname, 'edit.html')));

  return router;
};
