var config = require("config"),
  express = require("express"),
  resolve = require("path").resolve;

module.exports = function() {
  var router = express.Router();

  router.use("/", express.static(resolve(process.cwd(), config.get('site').publicPath)));

  return router;
};