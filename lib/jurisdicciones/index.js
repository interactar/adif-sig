var debug = require("debug")("adif:jurisdicciones");
var express = require("express");

module.exports = function jurisdicciones(app) {

  var router = express.Router();
  var db = app.get('db');

  router.get('/',
    // bodyParser.urlencoded({extended: true }),
    function(req, res) {
      debug('Listando jurisdicciones');
      db.selectjurisdicciones(function(err, result){
        res.send(result);
      });
    }
  );
  router.get('/:gid',
    // bodyParser.urlencoded({extended: true }),
    function(req, res) {
      debug('Leyendo jurisdiccion %s',req.params.gid);
      // db.jurisdicciones.findOne(parseInt(req.params.gid,10), function(err, jurisdiccion){
      db.getGeom(parseInt(req.params.gid,10), function(err, jurisdiccion){
        res.send(jurisdiccion);
      });
    }
  );
  return router;
};
