var config = require("config"),
  express = require("express"),
  debug = require('debug')('adif:proxy'),
  request = require('request');

var bodyParser = require('body-parser');

/**
 * Proxy sin security, hay que mejorarlo
 */
module.exports = function() {
  var router = express.Router();
  router.get('/', function(req, res, next) {
    var u = req.query.url;
    if (!u) {
      debug('Can\'t proxy without a target set on query.url');
      return res.status(500).send();
    }
    debug('GET %s', decodeURI(u));
    // console.log(require('url').parse(decodeURI(u)));
    var r = request(u);
    r.on('error', function(e) {
      debug("Error on HTTP request %s", e);
    });
    r.pipe(res).on('error', function(e) {
      debug("Error on HTTP request %s", e);
      return res.status(500).send(e);
    });
  });

  router.post('/',
    bodyParser.json({ type: function(req){return /json/.test(req.headers['content-type'])} }),
    // el raw se necesita para los reqs a wfs que salen en xml
    bodyParser.raw({ type: '*/xml' }),
    function(req, res, next) {
    var u = req.query.url;
    if (!u) {
      debug('Can\'t proxy without a target set on query.url');
      return res.status(500).send();
    }
    debug('POST %s', decodeURI(u));

    // request.post({url: decodeURI(u), json: req.body }).pipe(res);

    var r;
    if(!!~req.headers['content-type'].indexOf('json')) {
      debug('Parsing JSON request');
      r = request.post({
        url: decodeURI(u),
        json: req.body
      });
    // // }else if (!!~req.headers['content-type'].indexOf('xml')) {
    }else{
      debug('Parsing RAW request');
      r = request.post({
        url: decodeURI(u),
        body: req.body
      });
    }

    r.on('error', function(e) {
      debug("Error on HTTP request %s", e);
      return res.status(500).send(e);
    });
    r.pipe(res).on('error', function(e) {
      debug("Error on HTTP request %s", e);
      return res.status(500).send(e);
    });
  });

  return router;
};
