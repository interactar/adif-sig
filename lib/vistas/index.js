var config = require("config"),
  express = require("express"),
  path = require('path'),
  resolve = path.resolve;

var url = require('url');
var fs = require('fs');

module.exports = function(route) {
  var router = express.Router();

  router.use(function(req, res, next) {
    // console.log(req.url);
    if(req.headers.referer) {
      var url_parts = url.parse(req.headers.referer, true);
      if(url_parts && url_parts.query && url_parts.query.vista) {
        var file = path.basename(route);
        var ext = path.extname(route);
        var filename = file.substr(0,file.indexOf(ext));
        var script = filename + "_" + url_parts.query.vista + ext;
        var lePath = path.join(
          process.cwd(),
          config.get('site').publicPath,
          path.dirname(route),
          script);
          // console.log(lePath);
        if(fs.existsSync(lePath)) {
          fs.createReadStream(lePath).pipe(res);
          return;
        }
      }
    }
    next();
    // express.static(resolve(process.cwd(), config.get('site').publicPath,));
  });
  return router;
};
