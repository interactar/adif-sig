var debug = require('debug')('adif::wfsRequest:jurisdiccion');
var express = require("express");
var url = require('url');
// var request = require('request');
var bodyParser = require('body-parser');

// var wfs = require('wfs');
// var geofilter = require('../lib/geofilter');

module.exports = function wfsRequestJurisdiccion(app) {

  var db = app.get('db');
  var router = express.Router();
  router.post('/',
    bodyParser.urlencoded({extended: true }),
    function(req, res) {
      debug('Fetching geom for %s', req.body.gid);
      db.getGeometry(parseInt(req.body.gid,10), function(err, points){
        if(err) {
          console.log("Error on HTTP request %s", err);
          return res.status(500).send(err);
        }
        // console.log(jurisdiccion);
        var layers = req.body.layers;

        var polygon = jurisdiccion[0].geometry;

        //remember, long first!

        var wfsrequest = {
          host: "geoserver.staging.adif.interactar.com:80",
          pathname: "geoserver/wfs",
          protocol: "http",
          query: {
            request: "GetFeature",
            version: "1.0.0",
            service: "wfs",
            outputformat: "json",
            typename: layers,
            cql_filter: "WITHIN(the_geom,"+polygon+")"
          }
        };

        debug('WFS Request: %s', url.format(wfsrequest));
        var r = request(url.format(wfsrequest));
        r.on('error', function(e) {
          console.log("Error on HTTP request %s", e);
        });
        r.pipe(res).on('error', function(e) {
          console.log("Error on HTTP request %s", e);
          return res.status(500).send(e);
        });
      });


    });

  return router;

}
