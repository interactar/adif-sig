var
  debug = require('debug')('adif::wfsRequest:polygon')
  express = require("express"),
  url = require('url'),
  request = require('request'),
  config = require('config'),
  bodyParser = require('body-parser');


module.exports = function wfsRequestPolygon() {

  var router = express.Router();
  router.post('/',
    bodyParser.urlencoded({extended: true }),
    function(req, res) {
      // req.body.geometry?

      // if(!req.body.layers || !req.body.center || !req.body.distance) {
      //   debug('Requested buffer lacks some parameters');
      //   return res.status(500).send('Buffer query needs layers, center and distance to work');
      // }

      // var center = req.body.center.split(',').join(' ');
      // var distance = req.body.distance;
      var layers = req.body.layers;
      var polygon = req.body.polygon;

      //remember, long first!

      var wfsrequest = {
        host: config.get('geoserver').host,
        pathname: "geoserver/wfs",
        protocol: "http",
        query: {
          request: "GetFeature",
          version: "1.0.0",
          service: "wfs",
          outputformat: "json",
          typename: layers,
          propertyName: "nombre,tipo,descripcion",
          cql_filter: "INTERSECTS(the_geom,"+polygon+")"
        }
      };

      // debug('WFS Request: %s', url.format(wfsrequest));
      var r = request(url.format(wfsrequest));
      r.on('error', function(e) {
        console.log("Error on HTTP request %s", e);
      });
      r.pipe(res).on('error', function(e) {
        console.log("Error on HTTP request %s", e);
        return res.status(500).send(e);
      });

      // res.send({result:'ok'});
      //http://geoserver.staging.adif.interactar.com/geoserver/wfs
        // ?request=getfeature&
        // version=1.0.0&
        // service=wfs&
        // outputformat=json&
        // cql_filter=DWITHIN(the_geom,POINT(-57%20-35),1,meters)&typename=adif:OBRAS_PUNTOS
      //http://geoserver.staging.adif.interactar.com/geoserver/wfs?request=getfeature&version=1.0.0&service=wfs&outputformat=json&cql_filter=DWITHIN(the_geom,POINT(-57%20-35),1,meters)&typename=adif:OBRAS_PUNTOS,adif:OBRAS_LINEAS_V2,adif:Estaciones_Nacional_ADIF_v1



    });

  return router;

}
