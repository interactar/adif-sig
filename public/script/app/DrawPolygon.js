/* Copyright (c) 2006-2013 by OpenLayers Contributors (see authors.txt for
 * full list of contributors). Published under the 2-clause BSD license.
 * See license.txt in the OpenLayers distribution or repository for the
 * full text of the license. */


/**
 * @requires OpenLayers/Control.js
 * @requires OpenLayers/StyleMap.js
 * @requires OpenLayers/Feature/Vector.js
 * @requires OpenLayers/Layer/Vector.js
 */

/**
 * Class: OpenLayers.Control.DrawBuffer
 * The DrawFeature control draws point, line or polygon features on a vector
 * layer when active.
 *
 * Inherits from:
 *  - <OpenLayers.Control>
 */
OpenLayers.Control.DrawPolygon = OpenLayers.Class(OpenLayers.Control.DrawFeature, {

  /**
   * Property: layer
   * {<OpenLayers.Layer.Vector>}
   */
  layer: null,

  /**
   * Property: styleMap
   * {<OpenLayers.StyleMap>}
   */
  styleMap: null,

  /**
   * Property: callbacks
   * {Object} The functions that are sent to the handler for callback
   */
  callbacks: null,

  /** 
   * APIProperty: events
   * {<OpenLayers.Events>} Events instance for listeners and triggering
   *     control specific events.
   *
   * Register a listener for a particular event with the following syntax:
   * (code)
   * control.events.register(type, obj, listener);
   * (end)
   *
   * Supported event types (in addition to those from <OpenLayers.Control.events>):
   * featureadded - Triggered when a feature is added
   */

  /**
   * APIProperty: multi
   * {Boolean} Cast features to multi-part geometries before passing to the
   *     layer.  Default is false.
   */
  multi: false,

  /**
   * APIProperty: featureAdded
   * {Function} Called after each feature is added
   */
  featureAdded: function() {},

  /**
   * APIProperty: handlerOptions
   * {Object} Used to set non-default properties on the control's handler
   */

  /**
   * Constructor: OpenLayers.Control.DrawBuffer
   * 
   * Parameters:
   * options - {Object} 
   */
  initialize: function(options) {

    var _this = this;

    // create default styleMap
    this.styleMap = new OpenLayers.StyleMap({
      "default": new OpenLayers.Style(null, {
        rules: [
          new OpenLayers.Rule({
            symbolizer: Ext.apply({
              "Polygon": {
                strokeWidth: 2,
                strokeOpacity: 1,
                strokeColor: "#666666",
                fillColor: "white",
                fillOpacity: 0.3,
                strokeDashstyle: "dash"
              }
            }, this.symbolizers)
          })
        ]
      })
    });

    // create layer to draw the buffer
    this.layer = new OpenLayers.Layer.Vector("AOI", {
      styleMap: this.styleMap
    });

    // provide handler
    // var handlerProto = OpenLayers.Handler.RegularPolygon;

    this.polyOptions = {
      handlerOptions: {
        // sides: 40,
        // freehand: true,
        style: {
          strokeWidth: 2,
          strokeOpacity: 1,
          strokeColor: "#666666",
          fillColor: "blue",
          fillOpacity: 0.3,
          strokeDashstyle: "dash"
        }
      }
    };

    OpenLayers.Control.DrawFeature.prototype.initialize.apply(this, [this.layer, OpenLayers.Handler.Polygon, this.polyOptions]);

    this.handlerOptions = this.handlerOptions || {};
    this.handlerOptions.layerOptions = OpenLayers.Util.applyDefaults(
      this.handlerOptions.layerOptions, {
        renderers: this.layer.renderers,
        rendererOptions: this.layer.rendererOptions
      }
    );
    if (!("multi" in this.handlerOptions)) {
      this.handlerOptions.multi = this.multi;
    }
    // var sketchStyle = this.layer.styleMap && this.layer.styleMap.styles.temporary;
    // if(sketchStyle) {
    //     this.handlerOptions.layerOptions = OpenLayers.Util.applyDefaults(
    //         this.handlerOptions.layerOptions,
    //         {styleMap: new OpenLayers.StyleMap({"default": sketchStyle})}
    //     );
    // }
    // this.handler = new handlerProto(this, this.callbacks, this.handlerOptions);
    this._assignEventHandlers();
  },


  _assignEventHandlers: function() {
    var _this = this;

    // this.layer.events.register("beforefeatureadded", this.layer, function(evt){
    //   _this.layer.destroyFeatures();
    //   _this.layer.redraw();
    // });

    this.layer.events.register("featureadded", this.layer, function(evt) {

      _this.events.triggerEvent("aoi", {polygon:evt.feature});
    });
  },


  CLASS_NAME: "OpenLayers.Control.DrawPolygon"
});
