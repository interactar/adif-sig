//Ext.onReady(function() {
GeoExt.Lang.set("es");
var GEOSERVER_URL = "http://geoserver.staging.adif.interactar.com:80/geoserver";
// pink tile avoidance
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 5;
// make OL compute scale according to WMS spec
OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;


var myMask = new Ext.LoadMask(Ext.getBody(),{msg:'Cargando...'});

slider = new GeoExt.LayerOpacitySlider({
  width: 120,
  layer: null,
  aggressive: true,
  inverse: true,
  plugins: new GeoExt.LayerOpacitySliderTip({
    template: '<div>{opacity}%</div>'
  })
});

var northPanel = {
 id: "northpanel",
  xtype: "panel",
  region: "north",
  border: true,
  height: 60,
  items: [{
    html: '<div style="background-color: #FFF;">' +
      '<img title="ADIF" src="img/logo.png" style="height:60px" alt="ADIF">' +
      '<img title="Ministerio del interior" src="img/mininterior.nueva.png" style="height:60px;float:right" alt="Ministerio del interior">' +
      '</div>'
  }]
};

//centerPanel, el mapa
var centerPanel = {
  id: "centerpanel",
  xtype: "panel",
  layout: "fit",
  region: "center",
  items: ["mymap"],
  title: "ADIF :: GIS"
};

//westPanel, legend y layers
var westPanel = {
  id: "westcontainer",
  region: "west",
  xtype: "panel",
  // layout: "vbox",
  width: 275,
  // border: true,
  layout: "accordion",
  split: true,
  collapsible: true,
  collapseMode: "mini",
  header: false,
  //hideCollapseTool: true,
  defaults: {
    width: "100%",
    layout: "fit"
  },
  items: [
    {
      title: "Capas",
      // flex: .5,
      id: "layer_tree"
    }, {
      title: "Leyenda",
      // flex: .5,
      id: "legend"
    // }, {
    //   title: 'Capas test',
    //   id: 'layerManager'
    }
  ]
};

//southPanel, panel for the FeatureGrid
var southPanel = {
  region: "south",
  xtype: "panel",
  id: "south",
  height: 150,
  border: false,
  split: true,
  collapsible: true,
  collapseMode: "mini",
  collapsed: true,
  //hideCollapseTool: true,
  //header: false,
  title: "Atributos",
  layout: "fit"
};

var app = new gxp.Viewer({

  proxy: "/proxy?url=",

  portalConfig: {
    layout: "border",

    items: [
      northPanel,
      centerPanel,
      westPanel,
      southPanel
    ]
  },

  tools: [
    //---------- LAYER TREE ----------
    {
      ptype: "gxp_layertree",
      groups: tree_groups, // tree_groups.js
      id: "tree",
      outputConfig: {
        id: "tree",
        useArrows: true,
        autoScroll: true,
        animate: true,
        tbar: [],
        bbar: ["Transparencia:&nbsp;&nbsp;", slider]
          /*listeners: {
             "click": clickHandler
          }*/
      },
      outputTarget: "layer_tree"
    },
    //---------- LEGEND ----------
    {
      // aca hay que appender de alguna forma &legend_options=fontAntiAliasing:true;dpi:96;fontSize:12;
      ptype: "gxp_legend",
      outputTarget: "legend",
      outputConfig: {
        header: false,
        autoScroll: true,
        filter: function(record) {
          // console.log(record.getLayer().name);
          // console.log(record.getLayer().isBaseLayer);
          // return !record.getLayer().isBaseLayer;
          var tester = new RegExp(/ign/i);
          return record.getLayer().name && !tester.test(record.getLayer().name);
        }
      }
    },
    //------- tree.tbar --------
    {
      ptype: "gxp_addlayers",
      //addActionText: "Más...",
      actionTarget: "tree.tbar"
    }, {
      ptype: "gxp_removelayer",
      actionTarget: ["tree.tbar", "tree.contextMenu"]
    }, {
      ptype: "gxp_zoomtolayerextent",
      actionTarget: ["tree.tbar", "tree.contextMenu"]
    }, {
      ptype: "gxp_layerproperties",
      actionTarget: ["tree.tbar", "tree.contextMenu"]
    }, {
      ptype: "gxp_queryform",
      featureManager: "featuremanager",
      autoExpand: "south",
      outputConfig: {
        title: "Crear consulta",
        // width: "75%",
        width: "400",
        layout: "form"
      },

      actionTarget: "tree.tbar",
      queryActionText: "Buscar",
      queryByLocationText: "Buscar en la extensión actual del mapa"
    },

    //------- map.tbar ---------
    {
      ptype: "gxp_navigationhistory",
      actionTarget: "map.tbar"
    }, {
      ptype: 'gxp_navigation',
      actionTarget: "map.tbar",
      toggleGroup: "mapnav"
    }, {
      actions: ["-"],
      actionTarget: "map.tbar"
    }, {
      ptype: "gxp_zoomtoextent",
      actionTarget: "map.tbar",
      extent: new OpenLayers.Bounds(-12071848.595915, -8040865.4728296, -1867199.5731514, -2170501.7013452),
      tooltip: "Ver extensión inicial"
    }, {
      ptype: "gxp_zoom",
      showZoomBoxAction: true,
      actionTarget: "map.tbar",
      toggleGroup: "mapnav"
    }, {
      ptype: "gxp_googlegeocoder",
      outputTarget: "map.tbar",
      outputConfig: {
        emptyText: "Buscar un lugar ..."
      }
    }, {
      actions: ["-"],
      actionTarget: "map.tbar"
    }, {
      ptype: "gxp_wmsgetfeatureinfo",
      //format: "grid",
      outputConfig: {
        width: 550,
        height: 350,
        draggable: true
      },
      showButtonText: true,
      buttonText: "Info puntual",
      actionTarget: "map.tbar",
      layerParams: ['CQL_FILTER'],
      toggleGroup: "tools"
    }, {
      ptype: "gxp_measure",
      outputConfig: {
        width: 400,
        height: "auto"
      },
      showButtonText: true,
      actionTarget: "map.tbar",
      toggleGroup: "tools"
    }, {
      ptype: "gxp_spatialselector",
      buttonText: "Seleccionar...",
      bufferMenuText: "influencia",
      polygonMenuText: "polígono",
      selectTooltip: "Seleccionar objetos",
      controlOptions: {
        bufferCallback: function(evt){
          // feature en 4326
          var f = evt.feature.geometry.clone().transform('EPSG:3857','EPSG:4326');
          // centro en 4326
          var c = evt.center.clone().transform('EPSG:3857','EPSG:4326');

          // tengo que proveer la distancia en dd, asi lo requiere una capa en 4326
          // como eso es imposible y dado que estamos dibujando un circulo
          // genero un Point en la misma Lat que el center, pero con long en el bounds
          var distanceDD = Math.abs(c.x - f.getBounds().left);

          //layers
          var layers = app.mapPanel.map
            .getLayersByClass("OpenLayers.Layer.WMS")
            .filter(function(l){
              return l.params && !!~l.params.LAYERS.indexOf('adif') && l.visibility == true;
            })
            .map(function(l){
              return l.params.LAYERS;
            })
            .join(",");

          // var bufferWindow = createGridWindow();
          // store_buffer.setBaseParam('center',String(c.x) + "," + String(c.y));
          // store_buffer.setBaseParam('distance', distanceDD);
          // store_buffer.setBaseParam('layers', layers);
          var inf = 'Area de influencia: ' +
            (parseFloat(evt.distance) / 1000).toFixed(2) + ' km';
          var centro = 'Centro: ' + parseFloat(c.x).toFixed(3) + ', ' +
            parseFloat(c.y).toFixed(3);

          // bufferGrid.reconfigure(store_buffer, bufferGrid.getColumnModel());

          bufferWindow.setTitle(inf + '  |  ' + centro);
          bufferWindow.show();
          store_buffer.load({
            params: {
            'distance': distanceDD,
            'layers': layers,
            'center': String(c.x) + "," + String(c.y)
            },
            callback: function(){
              console.log('buffer store load callback');
            }
          });
        },
        polygonCallback: function(evt) {
          // geometry en 4326
          var g = evt.polygon.geometry.clone().transform('EPSG:3857','EPSG:4326');
          // centro en 4326
          // var c = evt.center.clone().transform('EPSG:3857','EPSG:4326');

          selectByPolygon(g);

        }
      },
      // outputConfig: {
      //   width: 400,
      //   height: "auto"
      // },
      showButtonText: true,
      actionTarget: "map.tbar",
      toggleGroup: "tools"
    }, {
      ptype: "gxp_print",
      printService: GEOSERVER_URL + "/pdf/",
      includeLegend: true,
      showButtonText: true,
      id: 'printer',
      // menuText: "Imprimir mapa",
      // tooltip: "Imprimir mapa",
      actionTarget: "map.tbar"
    // }, {
    //   actions: ["->"],
    //   actionTarget: "map.tbar"
    // }, {
    //   actions:["<a href=\"http://idef.formosa.gob.ar/?q=contact\" target=\"_blank\">Contáctenos...</a>"]
    }, {
      actions: ["-"],
      actionTarget: "map.tbar"
    },
    // selector de vista
    {
      actions:[
        new Ext.form.ComboBox({
          store: new Ext.data.ArrayStore({
            fields:['url', 'nombre'],
            data: [
              ['?vista=barosario','BsAs - Rosario'],
              ['?vista=bamdq','BsAs - MDQ'],
              ['?vista=obras_conservacion','Obras Conservacion'],
              ['?vista=belgrano_cargas_t','Belgrano Cargas T'],
              ['?vista=bamdz','BsAs - Mendoza']
            ]
          }),
          displayField: 'nombre',
          typeAhead: true,
          mode: 'local',
          triggerAction: 'all',
          emptyText:'Seleccionar vista...',
          // selectOnFocus:true,
          // width:100,
          listeners:{
            select: function(combo, item, index) {
              // console.log(arguments);
              // console.log(item.data);
              window.location = item.data.url;
            }
          }
        })
      ],
      actionTarget: 'map.tbar'
    },
    // selector por jurisdicciones
    {
      actions:[
        new Ext.form.ComboBox({
          store: new Ext.data.JsonStore({
            url: '/jurisdicciones',
            storeId: 'store_jurisdicciones',
            idProperty: 'gid',
            fields:['gid', 'nombre','jurisdiccion','tipo'],
            autoLoad: true
          }),
          displayField: 'nombre',
          // valueField: 'gid',
          loadingText: 'Buscando...',
          // restful: true,
          tpl: '<tpl for="."><div ext:qtip="{tipo}" class="x-combo-list-item">{nombre} - {jurisdiccion}</div></tpl>',
          typeAhead: true,
          mode: 'local',
          listAlign: ['tr-br?', [18, 0]],
          listWidth: 400,
          triggerAction: 'all',
          emptyText:'Jurisdicciones...',
          // selectOnFocus:true,
          // width:100,
          listeners:{
            select: function(combo, item, index) {
              // console.log(arguments);
              // console.log(combo);
              store_poly.vectorLayer.removeAllFeatures();
              Ext.Ajax.request({
                method: 'GET',
                loadMask: true,
                scope: this,
                url: "/jurisdicciones/"+item.id,
                success: function (response, request) {
                  var simplifierRate = 2;
                  var r = JSON.parse(response.responseText);
                  var title = item.data.nombre == item.data.jurisdiccion ?
                    "Provincia: " + item.data.jurisdiccion :
                    "Provincia: " + item.data.jurisdiccion + " - Departamento: " + item.data.nombre;
                  //selectByPolygon(r[0].geometry, "Selección :: " + title, true );

                  var g = new OpenLayers.Geometry.fromWKT(r[0].geometry);
                  var lr = new OpenLayers.Geometry.LinearRing(g.getVertices());
                  // lr.transform('EPSG:4326','EPSG:3857');
                  var s = lr.simplify(0.01);
                  console.log('Shape points length ' + s.components.length);
                  while(s.components.length > 65) {
                    console.log('Simplifying by '+ simplifierRate * 0.01);
                    s = lr.simplify(0.01 * simplifierRate);
                    simplifierRate += simplifierRate;
                  }
                  console.log('Shape simplified: '+s.components.length);
                  var p = new OpenLayers.Geometry.Polygon([
                    new OpenLayers.Geometry.LinearRing(s.getVertices())
                  ]);

                  selectByPolygon(p, title, true);

                },
                failure: function (response, request) {
                  Ext.MessageBox.alert('failure', response.responseText);
                  // console.log(response.responseText);
                }
              });

            }
          }
        })
      ],
      actionTarget: 'map.tbar'
    },

    {
      actions:["-","<a href=\"/edit.html\">Editar</a>"]
    },

    //---------  south grid --------------
    {
      // shared FeatureManager for feature editing, grid and querying
      ptype: "gxp_featuremanager",
      id: "featuremanager",
      //autoLoadFeatures: true,
      paging: false,
      maxFeatures: 100
    }, {
      ptype: "gxp_featuregrid",
      featureManager: "featuremanager",
      showTotalResults: true,
      // autoLoadFeature: true,
      //alwaysDisplayOnMap: true,
      id: "featureGrid",
      selectOnMap: true,
      outputConfig: {
        id: "featuregrid"
      },
      outputTarget: "south"
    }
  ],

  mapItems: [
    {
      xtype: "gx_zoomslider",
      vertical: true,
      height: 100
    }, {
      xtype: "gxp_scaleoverlay"
    }
  ],

  mapPlugins: {
    ptype: "gxp_loadingindicator",
    onlyShowOnFirstLoad: true,
    loadingMapMessage: "Cargando mapa..."
  },


  defaultSourceType: "gxp_wmscsource",

  sources: sources,

  map: Ext.apply({
    id: "mymap",
    //projection: "EPSG:4326",
    projection: "EPSG:3857",
    //units: "degrees",
    //center: [-60, -24.7],
    center: [-6697106, -5162855],
    zoom: 4,
    numZoomLevels: 20, //Para coincidir con los niveles de capas de Google
    zoomDuration: 10, //To match Google’s zoom animation better with OpenLayers animated zooming

    theme: 'theme/style.css',
    layers: layers, // layers.js
    stateId: 'myMap',
    prettyStateKeys: true
  }, mapparams || {})
});

function selectByPolygon(geom, windowTitle, zoomToPoly){
  //layers
  var layers = app.mapPanel.map
    .getLayersByClass("OpenLayers.Layer.WMS")
    .filter(function(l){
      return l.params && !!~l.params.LAYERS.indexOf('adif') && l.visibility == true;
    })
    .map(function(l){
      return l.params.LAYERS;
    })
    // .unique()
    .join(",");

  // var polyWindow = createGridWindow();
  // var inf = 'Area de influencia: ' +
  //   (parseFloat(evt.distance) / 1000).toFixed(2) + ' km';
  // var centro = 'Centro: ' + parseFloat(c.x).toFixed(3) + ', ' +
  //   parseFloat(c.y).toFixed(3);

  // resultsGrid.reconfigure(store_poly, resultsGrid.getColumnModel());

  polygonWindow.setTitle(windowTitle || "Selección por polígono");
  polygonWindow.show();
  store_poly.load({
    params: {
      polygon: geom,
      layers: layers
    },
    callback: function(){
      // console.log(store_poly);
      // console.log(this);
      if(zoomToPoly) {
        app.mapPanel.map.zoomToExtent(geom.transform('EPSG:4326','EPSG:3857').getBounds());
        app.mapPanel.map.addLayer(store_poly.vectorLayer);
        store_poly.vectorLayer.addFeatures([new OpenLayers.Feature.Vector(geom)]);
      }
    }
  });


}

// var vectorLayer = new OpenLayers.Layer.Vector("Selection");

btnMetadatos = new Ext.Button({
  text: 'Metadatos',
  tooltip: 'Acceso a metadatos de la capa',
  icon: './theme/info-new-window.png',
  disabled: true,
  handler: function(baseItem, e) {
    if (Ext.getCmp('ventanaMetadatos')) {
      Ext.getCmp('ventanaMetadatos').destroy();
    }
    new Ext.Window({
      title: 'Metadatos de la capa',
      id: 'ventanaMetadatos',
      maximizable: true,
      width: 800,
      height: 550,
      stateful: false,
      html: '<iframe src ="' + app.selectedLayer.data.metadataURLs[0]
        .href +
        '" width="100%" height="100%"></iframe>'
    }).show();
  }
});

app.mapPanel.map.addControl(getOverviewControl());

// app.mapPanel.map.addControl(new OpenLayers.Control.Permalink());

app.mapPanel.map.addControl(
  new OpenLayers.Control.MousePosition({

    formatOutput: function(lonLat) {
      markup = '<a target="_blank" ' +
        'href="http://spatialreference.org/ref/sr-org/7483/">' +
        'EPSG:3857</a> | ';

      point = lonLat.transform(new OpenLayers.Projection("EPSG:3857"),
        new OpenLayers.Projection("EPSG:4326"));

      markup += convertDMS(point.lat) + "," + convertDMS(point.lon);
      return markup;
    }
  })
);

app.on("ready", function() {

  // app.mapPanel.map.addLayer(vectorLayer);

  //inicializa el control y los handlers
  // initializeBufferSelector(app);

  tree = Ext.getCmp('tree');
  tree.on("click", function(node, e) {
    if (node.isLeaf()) {

      slider.setLayer(node.layer);

      // if (app.selectedLayer
      //   && app.selectedLayer.data
      //   && app.selectedLayer.data.metadataURLs
      //   && app.selectedLayer.data.metadataURLs[0]) {
      //   btnMetadatos.enable();
      // } else {
      //   btnMetadatos.disable();
      // }
    }
  });
  // treeTbar = tree.getTopToolbar();
  // treeTbar = Ext.getCmp('layer_tree').items.items[0].toolbars[0];
  // treeTbar.add(btnMetadatos);
  // treeTbar.doLayout();

  // si este boton lo pongo en el outputConfig de featuregrid sobrescribe el boton
  // por defecto de Mostrar resultados en mapa
  fgrid = Ext.getCmp('featuregrid');
  fgridBbar = fgrid.toolbars[0];
  fgridBbar.add({
    iconCls: "gxp-icon-zoom-to",
    ref: "../zoomToPageButton",
    text: "Zoom a resultados",
    tooltip: "Acercar mapa a la extensión de los resultados",
    handler: function() {
      app.mapPanel.map.zoomToExtent(fgrid.store.layer.getDataExtent());
    }
  });

  fgridBbar.items.items[1].toggle(); //boton Mostrar en mapa: on

});


function initEdit() {
  app.featureGrid = app.tools.featureGrid.output[0];
  app.tree = app.tools.tree.output[0];

  var vectorLayer = new OpenLayers.Layer.Vector("Editable features");
  app.mapPanel.map.addLayer(vectorLayer);

  var rawAttributeData;
  var read = OpenLayers.Format.WFSDescribeFeatureType.prototype.read;
  OpenLayers.Format.WFSDescribeFeatureType.prototype.read = function() {
      rawAttributeData = read.apply(this, arguments);
      return rawAttributeData;
  };

  function reconfigure(store, url) {
    var fields = [], columns = [], geometryName, geometryType;
    // regular expression to detect the geometry column
    var geomRegex = /gml:(Multi)?(Point|Line|Polygon|Surface|Geometry).*/;
    var types = {
      // mapping of xml schema data types to Ext JS data types
      "xsd:int": "int",
      "xsd:short": "int",
      "xsd:long": "int",
      "xsd:string": "string",
      "xsd:dateTime": "string",
      "xsd:double": "float",
      "xsd:decimal": "float",
      // mapping of geometry types
      "Line": "Path",
      "Surface": "Polygon"
    };
    store.each(function(rec) {
      var type = rec.get("type");
      var name = rec.get("name");
      var match = geomRegex.exec(type);
      if (match) {
        // we found the geometry column
        geometryName = name;
      } else {
        // we have an attribute column
        fields.push({
          name: name,
          type: types[type]
        });
        columns.push({
          xtype: types[type] == "string" ?
            "gridcolumn" :
            "numbercolumn",
          dataIndex: name,
          header: name
        });
      }
    });
    app.featureGrid.reconfigure(
      new GeoExt.data.FeatureStore({
        autoLoad: false,
        proxy: new GeoExt.data.ProtocolProxy({
          protocol: new OpenLayers.Protocol.WFS({
            url: url,
            version: "1.1.0",
            featureType: rawAttributeData.featureTypes[0].typeName,
            featureNS: rawAttributeData.targetNamespace,
            srsName: "EPSG:3857",
            geometryName: geometryName,
            maxFeatures: 250
          })
        }),
        fields: fields
      }),
      new Ext.grid.ColumnModel(columns)
    );
    app.featureGrid.store.bind(vectorLayer);
    app.featureGrid.getSelectionModel().bind(vectorLayer);
  }

  function setLayer(model, node) {
    if(!node || node.layer instanceof OpenLayers.Layer.Vector) {
      return;
    }
    vectorLayer.removeAllFeatures();
    app.featureGrid.reconfigure(
      new Ext.data.Store(),
      new Ext.grid.ColumnModel([])
    );
    var layer = node.layer;
    var url = layer.url.split("?")[0]; // the base url without params
    var schema = new GeoExt.data.AttributeStore({
      url: url,
      // request specific params
      baseParams: {
        "SERVICE": "WFS",
        "REQUEST": "DescribeFeatureType",
        "VERSION": "1.1.0",
        "TYPENAME": layer.params.LAYERS,
        "CQL_FILTER": layer.params.CQL_FILTER
      },
      autoLoad: false,
      listeners: {
        "load": function(store) {
          app.featureGrid.setTitle(layer.name);
          reconfigure(store, url);
        }
      }
    });
  }


  var modifyControl = new OpenLayers.Control.ModifyFeature(
    vectorLayer, {autoActivate: true}
  );
  var drawControl = new OpenLayers.Control.DrawFeature(
    vectorLayer,
    OpenLayers.Handler.Polygon,
    {handlerOptions: {multi: true}}
  );
  var controls = []; controls.push(modifyControl, drawControl);


  Ext.onReady(function() {
    app.tree.getSelectionModel().on(
      "selectionchange", setLayer
    );

    console.log(modifyControl);
    var sm = app.featureGrid.getSelectionModel();
    sm.unbind();
    sm.bind(modifyControl.selectControl);
    sm.on("beforerowselect", function() { sm.clearSelections(); });

    console.log('madness!');
      var bbar = app.featureGrid.getBottomToolbar();
      bbar.add([{
          text: "Delete",
          handler: function() {
              app.featureGrid.getSelectionModel().each(function(rec) {
                  var feature = rec.getFeature();
                  modifyControl.unselectFeature(feature);
                  vectorLayer.removeFeatures([feature]);
              });
          }
      }, new GeoExt.Action({
          control: drawControl,
          text: "Create",
          enableToggle: true
      })]);
      bbar.doLayout();
  });

  // esta inicializacion la esta haciendo el reconfigure la
  // primera vez que corre on.selectionchange
  // Ext.onReady(function() {
  //     app.mapPanel.map.addLayer(vectorLayer);
  //     app.featureGrid.store.bind(vectorLayer);
  //     app.featureGrid.getSelectionModel().bind(vectorLayer);
  // });
}
