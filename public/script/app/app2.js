
GeoExt.Lang.set("es");
var GEOSERVER_URL = "http://geoserver.staging.adif.interactar.com:80/geoserver";
// pink tile avoidance
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 5;
// make OL compute scale according to WMS spec
OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;


// Ext.BLANK_IMAGE_URL = "ext/resources/images/default/s.gif";
var app, items = [], controls = [];

var vectorLayer = new OpenLayers.Layer.Vector("Editable features");

/////// #1

controls.push(
  new OpenLayers.Control.Navigation(),
  new OpenLayers.Control.Attribution(),
  new OpenLayers.Control.PanPanel(),
  new OpenLayers.Control.ZoomPanel()
);

controls.push(new OpenLayers.Control.WMSGetFeatureInfo({
    autoActivate: true,
    infoFormat: "application/vnd.ogc.gml",
    maxFeatures: 10,
    drillDown: true,
    queryVisible: true,
    eventListeners: {
      "getfeatureinfo": function(e) {
          var items = [];
          Ext.each(e.features, function(feature) {
              items.push(new Ext.grid.PropertyGrid({
                  // xtype: "propertygrid",
                  title: feature.fid,
                  source: feature.attributes
              }));
          });
          new GeoExt.Popup({
              title: "Feature Info",
              width: 400,
              height: 300,
              layout: "accordion",
              map: app.mapPanel,
              location: e.xy,
              items: items
          }).show();
      }
    }
}));

var modifyControl = new OpenLayers.Control.ModifyFeature(
    vectorLayer, {standalone: false, autoActivate: true}
);
var drawControl = new OpenLayers.Control.DrawFeature(
    vectorLayer,
    OpenLayers.Handler.Polygon,
    {handlerOptions: {multi: true}}
);

controls.push(modifyControl, drawControl);

items.push(new GeoExt.MapPanel({

  ref: "mapPanel",
  region: "center",
  map: {
    id: "mymap",
    projection: "EPSG:3857",
    numZoomLevels: 20,
    theme: 'theme/style.css',
    controls: controls
  },
  center: [-7269467.0, -4999593.0],
  zoom: 4,
  // extent: OpenLayers.Bounds.fromArray([
  //     -64, -35,
  //     -122.787,42.398
  // ]),
  layers: [new OpenLayers.Layer.WMS(
    "FFCC",
    GEOSERVER_URL + "/wms?",
    {layers: "adif:ffcc_nacional_adif_v3", reproject: true},
    {isBaseLayer: false }
  )]
}));


items.push(new Ext.grid.GridPanel({
  // xtype: "grid",
  ref: "capsGrid",
  title: "Available Layers",
  region: "north",
  height: 150,
  viewConfig: {forceFit: true},
  store: new GeoExt.data.WMSCapabilitiesStore({
    url: GEOSERVER_URL + "/wms?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.1.1",
    autoLoad: true
  }),
  columns: [
    {header: "Name", dataIndex: "name", sortable: true},
    {header: "Title", dataIndex: "title", sortable: true},
    {header: "Abstract", dataIndex: "abstract"}
  ],
  bbar: [{
      text: "Add to Map",
      handler: function() {
          app.capsGrid.getSelectionModel().each(function(record) {
              var clone = record.clone();
              clone.getLayer().mergeNewParams({
                  format: "image/png",
                  transparent: true
              });
              app.mapPanel.layers.add(clone);
              app.mapPanel.map.zoomToExtent(
                  OpenLayers.Bounds.fromArray(clone.get("llbbox")).transform('EPSG:4326','EPSG:3857')
              );
          });
      }
  },
  {
    text: "Remove from Map",
    handler: function() {
        var node = app.tree.getSelectionModel().getSelectedNode();
        if (node && node.layer instanceof OpenLayers.Layer.WMS) {
            app.mapPanel.map.removeLayer(node.layer);
        }
    }
  }]
}));



items.push(new Ext.tree.TreePanel({
  // xtype: "treepanel",
  ref: "tree",
  region: "west",
  width: 200,
  autoScroll: true,
  enableDD: true,
  root: new GeoExt.tree.LayerContainer({
      expanded: true
  }),
  tbar: [{
    text: "Remove from Map",
    handler: function() {
      var node = app.tree.getSelectionModel().getSelectedNode();
      if (node && node.layer instanceof OpenLayers.Layer.WMS) {
        app.mapPanel.map.removeLayer(node.layer);
      }
    }
  }]
}));


// items.push(new GeoExt.LegendPanel({
//   // xtype: "gx_legendpanel",
//   region: "east",
//   width: 200,
//   autoScroll: true,
//   padding: 5
// }));


items.push(new Ext.grid.EditorGridPanel({
  // xtype: "grid",
  ref: "featureGrid",
  title: "Feature Table",
  region: "south",
  height: 150,
  sm: new GeoExt.grid.FeatureSelectionModel({singleSelect:true,autoPanMapOnSelection:true}),
  store: new GeoExt.data.FeatureStore({
    fields: [
      {name: "nombre", type: "string"},
      {name: "descripcion", type: "string"},
      {name: "tipo", type: "string"},
      {name: "corredor", type: "string"},
      {name: "detalle", type: "string"},
      {name: "tipotrocha", type: "string"},
      {name: "consesion", type: "string"},
      {name: "estadoserv", type: "string"},
      {name: "estado_via", type: "string"},
      {name: "region", type: "string"},
      {name: "tipo_serv", type: "string"},
      {name: "tipo_tracc", type: "string"}
    ],
    proxy: new GeoExt.data.ProtocolProxy({
      protocol: new OpenLayers.Protocol.WFS({
        url: GEOSERVER_URL + "/ows",
        version: "1.1.0",
        featureType: "ffcc_nacional_adif_v3",
        featureNS: "adifse.com.ar",
        srsName: "EPSG:3857"
      })
    }),
    autoLoad: false
  }),
  columns: [
    {header: "nombre", dataIndex: "nombre"},
    {header: "descripcion", dataIndex: "descripcion"},
    {header: "tipo", dataIndex: "tipo"},
    {header: "corredor", dataIndex: "corredor"},
    {header: "detalle", dataIndex: "tipo"},
    {header: "tipotrocha", dataIndex: "tipotrocha"},
    {header: "consesion", dataIndex: "consesion"},
    {header: "estadoserv", dataIndex: "estadoserv"},
    {header: "estado_via", dataIndex: "estado_via"},
    {header: "region", dataIndex: "region"},
    {header: "tipo_serv", dataIndex: "tipo_serv"},
    {header: "tipo_tracc", dataIndex: "tipo_tracc"}
    // {xtype: "numbercolumn", header: "number_fac", dataIndex: "number_fac"},
    // {xtype: "numbercolumn", header: "area", dataIndex: "area"},
    // {xtype: "numbercolumn", header: "len", dataIndex: "len"}
  ],
  bbar: []
}));


/////// #2

var rawAttributeData;
var read = OpenLayers.Format.WFSDescribeFeatureType.prototype.read;
OpenLayers.Format.WFSDescribeFeatureType.prototype.read = function() {
  rawAttributeData = read.apply(this, arguments);
  return rawAttributeData;
};

function reconfigure(store, url) {
    var fields = [], columns = [], geometryName, geometryType;
    // regular expression to detect the geometry column
    var geomRegex = /gml:(Multi)?(Point|Line|Polygon|Surface|Geometry).*/;
    // mapping of xml schema data types to Ext JS data types
    var types = {
        "xsd:int": "int",
        "xsd:short": "int",
        "xsd:long": "int",
        "xsd:string": "string",
        "xsd:dateTime": "string",
        "xsd:double": "float",
        "xsd:decimal": "float",
        "Line": "Path",
        "Surface": "Polygon"
    };
    store.each(function(rec) {
        var type = rec.get("type");
        var name = rec.get("name");
        var match = geomRegex.exec(type);
        if (match) {
            // we found the geometry column
            geometryName = name;
            // Geometry type for the sketch handler:
            // match[2] is "Point", "Line", "Polygon", "Surface" or "Geometry"
            geometryType = types[match[2]] || match[2];
        } else {
            // we have an attribute column
            fields.push({
                name: name,
                type: types[type]
            });
            columns.push({
                xtype: types[type] == "string" ?
                    "gridcolumn" :
                    "numbercolumn",
                dataIndex: name,
                header: name,
                // textfield editor for strings, numberfield for others
                editor: {
                    xtype: types[type] == "string" ?
                        "textfield" :
                        "numberfield"
                }
            });
        }
    });
    app.featureGrid.reconfigure(new GeoExt.data.FeatureStore({
        autoLoad: true,
        proxy: new GeoExt.data.ProtocolProxy({
            protocol: new OpenLayers.Protocol.WFS({
                url: url,
                version: "1.1.0",
                featureType: rawAttributeData.featureTypes[0].typeName,
                featureNS: rawAttributeData.targetNamespace,
                srsName: "EPSG:3857",
                geometryName: geometryName,
                maxFeatures: 250
            })
        }),
        fields: fields
    }), new Ext.grid.ColumnModel(columns));
    app.featureGrid.store.bind(vectorLayer);
    app.featureGrid.getSelectionModel().bind(vectorLayer);

    // Set the correct sketch handler according to the geometryType
    drawControl.handler = new OpenLayers.Handler[geometryType](
        drawControl, drawControl.callbacks, drawControl.handlerOptions
    );
}

function setLayer(model, node) {
    if(!node || node.layer instanceof OpenLayers.Layer.Vector) {
        return;
    }
    if(!node.isLeaf()) { return }
    vectorLayer.removeAllFeatures();
    app.featureGrid.reconfigure(
        new Ext.data.Store(),
        new Ext.grid.ColumnModel([])
    );
    var layer = node.layer;
    var url = layer.url.split("?")[0]; // the base url without params
    var schema = new GeoExt.data.AttributeStore({
        url: url,
        // request specific params
        baseParams: {
            "SERVICE": "WFS",
            "REQUEST": "DescribeFeatureType",
            "VERSION": "1.1.0",
            "TYPENAME": layer.params.LAYERS
        },
        autoLoad: true,
        listeners: {
            "load": function(store) {
                app.featureGrid.setTitle(layer.name);
                reconfigure(store, url);
            }
        }
    });
}
/////// #3




//kludge para openlayers 2.13 (funcionaba para la 2.10)
// var selectOptions = {
//     geometryTypes: modifyControl.geometryTypes,
//     clickout: modifyControl.clickout,
//     toggle: modifyControl.toggle,
//     onBeforeSelect: modifyControl.beforeSelectFeature,
//     onSelect: modifyControl.selectFeature,
//     onUnselect: modifyControl.unselectFeature,
//     scope: modifyControl,
//     style: vectorLayer.styleMap.styles.default
// };
// modifyControl.selectControl = new OpenLayers.Control.SelectFeature(vectorLayer, selectOptions);

modifyControl.selectControl = modifyControl;
modifyControl.selectControl.select = modifyControl.selectFeature;

var dragOptions = {
  geometryTypes: ["OpenLayers.Geometry.Point"],
  snappingOptions: modifyControl.snappingOptions,
  onStart: function(feature, pixel) {
      modifyControl.dragStart.apply(modifyControl, [feature, pixel]);
  },
  onDrag: function(feature, pixel) {
      modifyControl.dragVertex.apply(modifyControl, [feature, pixel]);
  },
  onComplete: function(feature) {
      modifyControl.dragComplete.apply(modifyControl, [feature]);
  },
  featureCallbacks: {
    over: function(feature) {
      /**
       * In normal mode, the feature handler is set up to allow
       * dragging of all points.  In standalone mode, we only
       * want to allow dragging of sketch vertices and virtual
       * vertices - or, in the case of a modifiable point, the
       * point itself.
       */
      if(modifyControl.standalone !== true || feature._sketch || modifyControl.feature === feature) {
          modifyControl.dragControl.overFeature.apply(modifyControl.dragControl, [feature]);
      }
    }
  }
};
modifyControl.dragControl = new OpenLayers.Control.DragFeature(
    vectorLayer, dragOptions
);




/////// #4


// #1
Ext.onReady(function() {
  app = new Ext.Viewport({
    proxy: "/proxy?url=",
    layout: "border",
    items: items,
    listeners: {
      afterrender: function(){
        console.log('ready');
        console.log(this);
        postInit(this);
      }
    }
  });

});
function postInit(app){
// #2
  app.mapPanel.map.addLayer(vectorLayer);
  app.featureGrid.store.bind(vectorLayer);
  app.featureGrid.getSelectionModel().bind(vectorLayer);
// #3
  app.tree.getSelectionModel().on(
    "selectionchange", setLayer
  );
// #4
  var sm = app.featureGrid.getSelectionModel();
  sm.unbind();
  sm.bind(modifyControl);
  sm.on("beforerowselect", function() { sm.clearSelections(); });

  var bbar = app.featureGrid.getBottomToolbar();
  bbar.add([
    {
      text: "Delete",
      // handler: function() {
      //   app.featureGrid.getSelectionModel().each(function(rec) {
      //     var feature = rec.getFeature();
      //     modifyControl.unselectFeature(feature);
      //     vectorLayer.removeFeatures([feature]);
      //   });
      // }

      handler: function() {
        app.featureGrid.store.featureFilter = new OpenLayers.Filter({
          evaluate: function(feature) {
            return feature.state != OpenLayers.State.DELETE;
          }
        });
        app.featureGrid.getSelectionModel().each(function(rec) {
          var feature = rec.getFeature();
          // console.log(feature);
          // feature.style = feature.layer.styleMap.styles.default.clone();
          modifyControl.unselectFeature(feature);
          vectorLayer.removeFeatures([feature]);
          if (feature.state != OpenLayers.State.INSERT) {
            feature.state = OpenLayers.State.DELETE;
            vectorLayer.addFeatures([feature]);
          }
        });
      }

    },
    new GeoExt.Action({
      control: drawControl,
      text: "Create",
      enableToggle: true
    }),
    {
      text: "Save",
      handler: function() {
        app.featureGrid.store.proxy.protocol.commit(vectorLayer.features,
          {
            callback: function() {
              var layers = app.mapPanel.map.layers;
              for (var i=layers.length-1; i>=0; --i) {
                layers[i].redraw(true);
              }
              app.featureGrid.store.reload();
            }
          }
        );
      }
    }
  ]);
  bbar.doLayout();
}
