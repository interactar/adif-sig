function onBuffer(evt){

  // feature en 4326
  var f = evt.feature.geometry.clone().transform('EPSG:3857','EPSG:4326');
  // centro en 4326
  var c = evt.center.clone().transform('EPSG:3857','EPSG:4326');

  // tengo que proveer la distancia en dd, asi lo requiere una capa en 4326
  // como eso es imposible y dado que estamos dibujando un circulo
  // genero un Point en la misma Lat que el center, pero con long en el bounds
  var distanceDD = Math.abs(c.x - f.getBounds().left);

  //layers
  var layers = app.mapPanel.map
    .getLayersByClass("OpenLayers.Layer.WMS")
    .filter(function(l){
      return l.params && !!~l.params.LAYERS.indexOf('adif') && l.visibility == true;
    })
    .map(function(l){
      return l.params.LAYERS;
    })
    .join(",");
  console.log(layers);
  // var bufferWindow = createGridWindow();
  // store_test.setBaseParam('center',String(c.x) + "," + String(c.y));
  // store_test.setBaseParam('distance', distanceDD);
  // store_test.setBaseParam('layers', layers);
  var inf = 'Area de influencia: ' +
    (parseFloat(evt.distance) / 1000).toFixed(2) + ' km';
  var centro = 'Centro: ' + parseFloat(c.x).toFixed(3) + ', ' +
    parseFloat(c.y).toFixed(3);
  resultsGridWindow.setTitle(inf + '  |  ' + centro);
  resultsGridWindow.show();
  store_test.load({
    params: {
    'distance': distanceDD,
    'layers': layers,
    'center': String(c.x) + "," + String(c.y)
    },
    callback: function(){
    }
  });
  // store_exp.load({
  //   params: {
  //     'distance': distanceDD,
  //     'layers': layers,
  //     'center': String(c.x) + "," + String(c.y)
  //   },
  //   callback: function(){
  //   }
  // });
}
function onPolygon(evt) {
  console.log('poly select');
  console.log(evt);
}
function initializeBufferSelector(app) {
  // var areaSelector = new OpenLayers.Control.DrawPolygon();
  // areaSelector.events.register('aoi', areaSelector, function(evt){
  //   console.log(evt.polygon);
  // });
  // app.mapPanel.map.addControl(areaSelector);

  var bufferControl = new OpenLayers.Control.DrawBuffer();
  bufferControl.events.register('buffer', bufferControl, onBuffer);


  // bufferControl.events.register('deactivate', bufferControl, function(evt){
  //   app.mapPanel.map.removeLayer(app.mapPanel.map.getLayersByName('BufferResults').pop());
  // });
  // bufferControl.events.register('activate', bufferControl, function(evt){
  //   app.mapPanel.map.addLayer(new OpenLayers.Layer.Vector('BufferResults'));
  // });


  var bufferButton = new Ext.Button({
    text: 'Buffer',
    enableToggle: true,
    toggleGroup: 'tools',
    handler: function(thisButton, evt){
      if (thisButton.pressed) {
        bufferControl.activate();
      } else {
        bufferControl.deactivate();
      }
    }
  });
  app.mapPanel.getTopToolbar().addButton(bufferButton);

  // le hack para que se vea el boton, sino no se updatea la toolbar
  // app.mapPanel.getTopToolbar().syncSize();
  app.mapPanel.getTopToolbar().doLayout();

  app.mapPanel.map.addControl(bufferControl);

}


function makeSandwich(){
  var addActions = function() {
    this.activeIndex = 0;
    this.button = new Ext.SplitButton({
      iconCls: "gxp-icon-measure-length",
      tooltip: this.measureTooltip,
      buttonText: this.buttonText,
      enableToggle: true,
      toggleGroup: this.toggleGroup,
      allowDepress: true,
      handler: function(button, event) {
        if(button.pressed) {
          button.menu.items.itemAt(this.activeIndex).setChecked(true);
        }
      },
      scope: this,
      listeners: {
        toggle: function(button, pressed) {
          // toggleGroup should handle this
          if(!pressed) {
            button.menu.items.each(function(i) {
              i.setChecked(false);
            });
          }
        },
        render: function(button) {
          // toggleGroup should handle this
          Ext.ButtonToggleMgr.register(button);
        }
      },
      menu: new Ext.menu.Menu({
        items: [
          new Ext.menu.CheckItem(
            new GeoExt.Action({
              text: this.lengthMenuText,
              iconCls: "gxp-icon-measure-length",
              toggleGroup: this.toggleGroup,
              group: this.toggleGroup,
              listeners: {
                checkchange: function(item, checked) {
                  this.activeIndex = 0;
                  this.button.toggle(checked);
                  if (checked) {
                    this.button.setIconClass(item.iconCls);
                  }
                },
                scope: this
              },
              map: this.target.mapPanel.map,
              control: this.createMeasureControl(
                OpenLayers.Handler.Path, this.lengthTooltip
              )
            })
          ),
          new Ext.menu.CheckItem(
            new GeoExt.Action({
              text: this.areaMenuText,
              iconCls: "gxp-icon-measure-area",
              toggleGroup: this.toggleGroup,
              group: this.toggleGroup,
              allowDepress: false,
              listeners: {
                checkchange: function(item, checked) {
                  this.activeIndex = 1;
                  this.button.toggle(checked);
                  if (checked) {
                    this.button.setIconClass(item.iconCls);
                  }
                },
                scope: this
              },
              map: this.target.mapPanel.map,
              control: this.createMeasureControl(
                OpenLayers.Handler.Polygon, this.areaTooltip
              )
            })
          )
        ]
      })
    });

    return gxp.plugins.Measure.superclass.addActions.apply(this, [this.button]);
  }

}