/*
Valid config options for all layer sources:

    source: String referencing a source from sources
    name: String - the name from the source’s store (only for sources that maintain a store)
    visibility: Boolean - initial layer visibility
    opacity: Number - initial layer.opacity
    group: String - group for the layer when the viewer also uses a gxp.plugins.LayerTree. Set this to “background” to make the layer a base layer
    fixed: Boolean - Set to true to prevent the layer from being removed by a gxp.plugins.RemoveLayer tool and from being dragged in a gxp.plugins.LayerTree
    selected: Boolean - Set to true to mark the layer selected
*/

var layers = [

  //------- Capas base
  {
    source: "ign",
    name: "capabaseargenmap",
    title: "IGN: Capa base Argenmap",
    group: "background",
    isBaseLayer: true,
    opacity: 0.25,
    visibility: false
  },
  // {
  //     source: "ign",
  //     name: "ideign:LIMITE_POLITICO_ADMINISTRATIVO",
  //     title: "IGN: Límite Político Administrativo",
  //     group: "background",
  //     fixed: true
  // },
  {
    source: "ol",
    group: "background",
    fixed: true,
    type: "OpenLayers.Layer",
    args: ["En blanco"],
    visibility: false
  },
  {
    source: "ol",
    group: "background",
    fixed: false,
    type: "OpenLayers.Layer.XYZ",
    args: [
      "CartoDB Dark Matter",
      "http://a.basemaps.cartocdn.com/dark_all/${z}/${x}/${y}.png",
       {attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'}
    ],
    visibility: false
  },
  {
    source: "ol",
    group: "background",
    fixed: true,
    type: "OpenLayers.Layer.XYZ",
    args: [
      "CartoDB Positron",
      "http://a.basemaps.cartocdn.com/light_all/${z}/${x}/${y}.png",
      {attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'}
    ],
    visibility: false
  },
  {
    source: "google",
    name: "ROADMAP",
    group: "background",
    visibility: false
  },
  {
    source: "google",
    name: "HYBRID",
    group: "background",
    visibility: false
  },
  {
    source: "google",
    name: "SATELLITE",
    group: "background",
    MAX_ZOOM_LEVEL: 21,
    visibility: false
  },
  {
      source: "mapquest",
      name: "osm",
      visibility: true
  },
  {
      source: "osm",
      name: "mapnik",
      visibility: false
  },

  //----- RASTER
  // {
  //     source: "dgct",
  //     name: "0",
  //     group: "raster",
  //     visibility:false,
  //     tiled: false
  // }, {
  //     source: "dpv",
  //     name: "dpv_prov_satelital",
  //     group: "raster",
  //     visibility:false
  // }, {
  //     source: "dpv",
  //     name: "dpv_aybal_rasterhd",
  //     group: "raster",
  //     visibility:false
  // }, {
  //     source: "dpv",
  //     name: "dpv_pilco_rasterhd",
  //     group: "raster",
  //     visibility:false
  // }, {
  //     source: "dpv",
  //     name: "dpv_lomitas_rasterhd",
  //     group: "raster",
  //     visibility:false
  // },

  //----- POLIGONOS
  // {
  //     source: "dgct",
  //     name: "2",
  //     group: "cartografia",
  //     visibility:false,
  //     tiled: false
  // },  {
  //     source: "dgct",
  //     name: "3",
  //     group: "cartografia",
  //     visibility:false,
  //     tiled: false
  // }, {
  //     source: "dgct",
  //     name: "4",
  //     group: "cartografia",
  //     visibility:false,
  //     tiled: false
  // },  {
  //     source: "dgct",
  //     name: "5",
  //     group: "cartografia",
  //     visibility:false,
  //     tiled: false
  // }, {
  //     source: "dgct",
  //     name: "6",
  //     group: "cartografia",
  //     visibility:false,
  //     tiled: false
  // }, {
  //     source: "dec",
  //     name: "dec_prov_censo2010",
  //     group: "estadistica",
  //     title:"Censo 2010 por Radio censal (sujeto a revisión)",
  //     visibility:false
  // }, {
  //     source: "dec",
  //     name: "dec_prov_censo2010xdpto",
  //     group: "estadistica",
  //     title:"Censo 2010 por Dpto. (sujeto a revisión)",
  //     vi  // {
  //     source: "bing",
  //     name: "AerialWithLabels"
  // }, {me: "upca_prov_cuencashid",
  //     group: "hidrografia",
  //     visibility:false
  // }, {
  //     source: "upca",
  //     name: "upca_prov_lagembestbania",
  //     group: "hidrografia",
  //     visibility:false
  // }, {
  //     source: "upca",
  //     name: "upca_prov_sistbanialaest",
  //     group: "hidrografia",
  //     visibility:false
  // }, {
  //     source: "mce",
  //     name: "mce_prov_delzonales",
  //     group: "educacion",
  //     title:"Delegaciones Zonales",
  //     visibility:false
  // },


    //-----  LINEAS FFCC
    {
      source: "adif",
      title: "Internacional",
      styles: 'linea_ffcc_internacional',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = 'FFCC Internacional'",
      group: "ffcc"
    },
    {
      source: "adif",
      title: "Provincial",
      styles: 'linea_ffcc_provincial',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = 'FFCC Provincial'",
      group: "ffcc"
    },
    {
      source: "adif",
      title: "Sin nombre",
      styles: 'linea_via_css',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = '' or nombre is null",
      group: "ffcc"
    },
    {
      source: "adif",
      title: "San Martín",
      styles: 'linea_ffcc_sanmartin',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = 'FFCC San Martín'",
      group: "ffcc"
    },
    {
      source: "adif",
      title: "Urquiza",
      styles: 'linea_ffcc_urquiza',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = 'FFCC Urquiza'",
      group: "ffcc"
    },
    {
      source: "adif",
      title: "Mitre",
      styles: 'linea_ffcc_mitre',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = 'FFCC Mitre'",
      group: "ffcc"
    },
    {
      source: "adif",
      title: "Belgrano",
      styles: 'linea_ffcc_belgrano',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = 'FFCC Belgrano'",
      group: "ffcc"
    },
    {
      source: "adif",
      title: "Sarmiento",
      styles: 'linea_ffcc_sarmiento',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = 'FFCC Sarmiento'",
      group: "ffcc"
    },
    {
      source: "adif",
      title: "Roca",
      styles: 'linea_ffcc_roca',
      name: "adif:ffcc_nacional_adif_v3",
      cql_filter: "nombre = 'FFCC Roca'",
      group: "ffcc"
    },


  // ------- OBRAS
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    styles: "linea_obras_otras",
    visibility: false,
    title: "Otras (L)",
    cql_filter: "tipo = 'Otras'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    styles: "punto_obras_otras",
    visibility: false,
    title: "Otras (P)",
    cql_filter: "tipo = 'Otras'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    styles: "linea_obras_renovacionDeVia",
    visibility: false,
    title: "Renovación de Vía (L)",
    cql_filter: "tipo = 'Renovación de vías'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    styles: "punto_obras_renovacionDeVia",
    visibility: false,
    title: "Renovación de Vía (P)",
    cql_filter: "tipo = 'Renovación de Vía'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    styles: "linea_obras_senalamientoTelecomunicaciones",
    visibility: false,
    title: "Señalamiento y Telecomunicaciones (L)",
    cql_filter: "tipo = 'Señalamiento y Telecomunicaciones'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    styles: "punto_obras_senalamientoTelecomunicaciones",
    visibility: false,
    title: "Señalamiento y Telecomunicaciones (P)",
    cql_filter: "tipo = 'Señalamiento y Telecomunicaciones'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    styles: "linea_obras_electrificacionRepotenciacion",
    visibility: false,
    title: "Electrificación y Repotenciación (L)",
    cql_filter: "tipo = 'Electrificación y Repotenciación'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    styles: "punto_obras_electrificacionRepotenciacion",
    visibility: false,
    title: "Electrificación y Repotenciación (P)",
    cql_filter: "tipo = 'Electrificación y Repotenciación'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    styles: "linea_obras_civiles",
    visibility: false,
    title: "Obras Civiles (L)",
    cql_filter: "tipo = 'Obra Civil'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    styles: "punto_obras_civiles",
    visibility: false,
    title: "Obras Civiles (P)",
    cql_filter: "tipo = 'Obra Civil'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    styles: "linea_obras_mejoramientoDeVia",
    visibility: false,
    title: "Mejoramiento de Vía (L)",
    cql_filter: "tipo = 'Mejoramiento de vías'",
    group: "obras_tipo"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    styles: "punto_obras_estaciones",
    visibility: false,
    title: "Estaciones (P)",
    cql_filter: "tipo = 'Estaciones'",
    group: "obras_tipo"
  },


  /// OBRAS ESTADO
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "Adjudicada (L)",
    cql_filter: "estado = 'Adjudicada'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "Adjudicada (P)",
    cql_filter: "estado = 'Adjudicada'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "En Ejecución (L)",
    cql_filter: "estado = 'En Ejecución'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "En Ejecución (P)",
    cql_filter: "estado = 'En Ejecución'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "En Licitación (L)",
    cql_filter: "estado = 'En Licitación'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "En Licitación (P)",
    cql_filter: "estado = 'En Licitación'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "En Proyecto (L)",
    cql_filter: "estado = 'En proyecto'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "En Proyecto (P)",
    cql_filter: "estado = 'En proyecto'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_lineas_v8",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "Finalizada (L)",
    cql_filter: "estado = 'Finalizada'",
    group: "obras_estado"
  },
  {
    source: "adif",
    name: "adif:obras_puntos_v6",
    // styles: "punto_obras_estaciones",
    visibility: false,
    title: "Finalizada (P)",
    cql_filter: "estado = 'Finalizada'",
    group: "obras_estado"
  },

 // Adjudicada    |    63
 // En Ejecución  |   334
 // En Licitación |   198
 // En proyecto   |   247
 // Finalizada    |   156



  // ESTADO DE LINEAS FFCC
  {
    source: "adif",
    name: "adif:ffcc_nacional_adif_v3",
    styles: "linea_estado_noOperativa",
    visibility: false,
    title: "No operativa",
    cql_filter: "estadoserv <> 'Operativa'",
    group: "estado"
  },
  {
    source: "adif",
    name: "adif:ffcc_nacional_adif_v3",
    styles: "linea_estado_operativa",
    visibility: false,
    title: "Operativa",
    cql_filter: "estadoserv = 'Operativa'",
    group: "estado"
  },



  {
    source: "adif",
    name: "adif:ffcc_nacional_adif_v3",
    styles: "linea_servicio",
    visibility: false,
    title: "Desconocido",
    cql_filter: "tipo_serv = '' or tipo_serv is null",
    group: "tipo"
  },
  {
    source: "adif",
    name: "adif:ffcc_nacional_adif_v3",
    styles: "linea_servicio_ambos",
    visibility: false,
    title: "Ambos",
    cql_filter: "tipo_serv = 'Ambos'",
    group: "tipo"
  },
  {
    source: "adif",
    name: "adif:ffcc_nacional_adif_v3",
    styles: "linea_servicio_carga",
    visibility: false,
    title: "Carga",
    cql_filter: "tipo_serv = 'Carga'",
    group: "tipo"
  },
  {
    source: "adif",
    name: "adif:ffcc_nacional_adif_v3",
    styles: "linea_servicio_pasajeros",
    visibility: false,
    title: "Pasajeros",
    cql_filter: "tipo_serv = 'Pasajeros'",
    group: "tipo"
  },


  {
    source: "adif",
    title: "Estaciones",
    name: "adif:estaciones_nacional_adif_v3",
    group: "estaciones"
  },


  {
    source: "ign",
    title: "Localidades IGN",
    name: "ideign:localidades",
    visibility: false,
    group: "indec"
  }

  // }, {
  //     source: "dpv",
  //     name: "dpv_prov_rutasnac",
  //     group: "viasdecom"
  // }, {
  //     source: "dpv",
  //     name: "dpv_prov_distritosdpv",
  //     group: "viasdecom",
  //     visibility:false
  // },  {
  //     source: "ign",
  //     name: "ideign:red_vial",
  //     group: "ign",
  //     visibility:false
  // }, {
  //     source: "ign",
  //     name: "ideign:RED_FERROVIARIA",
  //     group: "ign",
  //     visibility:false
  // }, {
  //     source: "upca",
  //     name: "upca_prov_canalcorred",
  //     group: "hidrografia",
  //     visibility:false
  // }, {
  //     source: "upca",
  //     name: "upca_prov_riacharroy",
  //     group: "hidrografia",
  //     visibility:false
  // }, {
  //     source: "upca",
  //     name: "upca_prov_riosppales",
  //     group: "hidrografia",
  //     visibility:false
  // },
  //----- PUNTOS
  // {
  //     source: "mce",
  //     name: "mce_prov_escuelas",
  //     group: "educacion",
  //     title:"Unidades Educativas (sujeto a revisión)",
  //     visibility:false
  // }, {
  //     source: "upca",
  //     name: "upca_prov_redhidrom",
  //     group: "hidrografia",
  //     visibility:false
  // }, {
  //     source: "upca",
  //     name: "upca_prov_redpluviom",
  //     group: "hidrografia",
  //     visibility:false
  // }, {
  //     source: "ign",
  //     name: "ideign:localidades",
  //     group: "ign",
  //     visibility: false
  // }, {
  //     source: "mdh",
  //     name: "prov_centrossalud",
  //     group: "salud",
  //     visibility:false
  // },
  //----- LABELS
  // {
  //     source: "dgct",
  //     name: "1",
  //     group: "cartografia",
  //     visibility:false,
  //     tiled: false
  // }
];
