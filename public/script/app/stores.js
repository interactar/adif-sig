

Ext.QuickTips.init();

store_buffer = new Ext.data.Store({
  url: '/wfsbufferselect',
  reader: new Ext.data.JsonReader({
    root: 'features',
    // idProperty: 'id',
    totalProperty: 'totalFeatures',
    fields: [
      // {name: 'id'},
      {name: 'Tipo', mapping: 'properties.tipo'},
      {name: 'Nombre', mapping: 'properties.nombre'},
      {name: 'Descripcion', mapping: 'properties.descripcion'}
    ]
  }),
  storeId: 'myBufferStore'
});

store_poly = new Ext.data.Store({
  url: '/wfspolygonselect',
  reader: new Ext.data.JsonReader({
    root: 'features',
    // idProperty: 'id',
    totalProperty: 'totalFeatures',
    fields: [
      // {name: 'id'},
      {name: 'Tipo', mapping: 'properties.tipo'},
      {name: 'Nombre', mapping: 'properties.nombre'},
      {name: 'Descripcion', mapping: 'properties.descripcion'}
    ]
  }),
  storeId: 'myPolyStore',
  vectorLayer: new OpenLayers.Layer.Vector("Jurisdiccion")
});

store_jurisdicciones = new Ext.data.Store({
  url: '/wfsjurisdiccion',
  reader: new Ext.data.JsonReader({
    root: 'features',
    // idProperty: 'id',
    totalProperty: 'totalFeatures',
    fields: [
      // {name: 'id'},
      // {name: 'Tipo', mapping: 'geometry.type'},
      {name: 'Nombre', mapping: 'properties.name'},
      {name: 'ffcc', mapping: 'properties.nombre'}
    ]
  }),
  storeId: 'myJuriStore'
});


// grid para los resultados del buffer
var bufferGrid = new Ext.grid.GridPanel({
  store: store_buffer,
  id: 'bufferGrid',
  tbar : [{
    xtype: 'exportbutton',
    store: store_buffer,
    exportFunction: 'exportStore',
    text: 'Exportar a Excel',
    title: 'ADIF',
    filename: 'buffer.xls'
  }],
  // plugins: expander,
  loadMask: {msg:'Obteniendo datos...'},
  enableHdMenu: true,
  viewConfig: {forceFit: true},
  // startCollapsed: true,
  enableColumnResize : true,
  columns: [
    // {header: 'ID', sortable: false, dataIndex: 'id'},
    {header: 'Nombre', sortable: true, dataIndex: 'Nombre'},
    {header: 'Tipo', sortable: true, dataIndex: 'Tipo'},
    {header: 'Descripcion', sortable: true, dataIndex: 'Descripcion'}
  ],
  frame: false,
  width: 395,
  height: 480,
  autowidth: true,
  collapsible: false,
  border: false,
  animCollapse: false,
  iconCls: 'icon-grid',
  // listeners : {
  //   onRender : function() {
  //     // let it render first
  //     bufferGrid.superclass.onRender.apply(this, arguments);

  //     //do it after the gridView is initialized !!
  //     //this.view.refresh();
  //     this.loadMask.show();
  //     console.log(this);
  //     // defer the store.load so browser can catch up
  //     this.store_test.load();

  //   }
  // }
});

// window para el buffer grid
var bufferWindow = new Ext.Window({
  id: 'bufferWindow',
  title: "Area de influencia",
  width: "75%",
  scripts: true,
  loadScripts: true,
  height: "50%",
  layout: 'fit',
  closable: true,
  closeAction: 'hide',
  constrain: true,
  shadow: false,
  buttonAlign: 'center',
  resizable: true,
  items: [bufferGrid],
  listeners: {
    close: function(){
      console.log('buffer Window closed callback');
      // console.log(arguments);
    }
  }
  // buttons: [
  //   {
  //     text: 'Cerrar',
  //     handler: function() {
  //       polygonLayer.destroyFeatures();
  //       polygonLayer.redraw();
  //       store_data_busqueda.removeAll();
  //       store_data_export.removeAll();
  //       var layers = map.getLayersByName('Centro');
  //       for (var layerIndex = 0; layerIndex < layers.length; layerIndex++) {
  //         map.removeLayer(layers[layerIndex]);
  //       }
  //       var layers = map.getLayersByName('Busca');
  //       for (var layerIndex = 0; layerIndex < layers.length; layerIndex++) {
  //         map.removeLayer(layers[layerIndex]);
  //       }
  //       bufferWindow.hide();
  //     }
  //   }
  // ]
});

// grid para los resultados del polygon select
var polygonGrid = new Ext.grid.GridPanel({
  store: store_poly,
  id: 'polygonGrid',
  tbar : [{
    xtype: 'exportbutton',
    store: store_poly,
    exportFunction: 'exportStore',
    text: 'Exportar a Excel',
    title: 'ADIF',
    filename: 'seleccion.xls'
  }],
  // plugins: expander,
  loadMask: {msg:'Obteniendo datos...'},
  enableHdMenu: true,
  viewConfig: {forceFit: true},
  // startCollapsed: true,
  enableColumnResize : true,
  columns: [
    // {header: 'ID', sortable: false, dataIndex: 'id'},
    {header: 'Nombre', sortable: true, dataIndex: 'Nombre'},
    {header: 'Tipo', sortable: true, dataIndex: 'Tipo'},
    {header: 'Descripcion', sortable: true, dataIndex: 'Descripcion'}
  ],
  frame: false,
  width: 395,
  height: 480,
  autowidth: true,
  collapsible: false,
  border: false,
  animCollapse: false,
  iconCls: 'icon-grid',
  // listeners : {
  //   onRender : function() {
  //     // let it render first
  //     bufferGrid.superclass.onRender.apply(this, arguments);

  //     //do it after the gridView is initialized !!
  //     //this.view.refresh();
  //     this.loadMask.show();
  //     console.log(this);
  //     // defer the store.load so browser can catch up
  //     this.store_test.load();

  //   }
  // }
});

// window para el polygon grid
var polygonWindow = new Ext.Window({
  id: 'polygonWindow',
  title: "Selección por polígono",
  width: "75%",
  height: "50%",
  layout: 'fit',
  closable: true,
  closeAction: 'hide',
  constrain: true,
  shadow: false,
  buttonAlign: 'center',
  resizable: true,
  items: [polygonGrid],
  listeners: {
    hide: function(windowObj){
      // console.log('Window closed');
      store_poly.vectorLayer.removeAllFeatures();
      app.mapPanel.map.removeLayer(store_poly.vectorLayer);
    }
  }
  // buttons: [
  //   {
  //     text: 'Cerrar',
  //     handler: function() {
  //       polygonLayer.destroyFeatures();
  //       polygonLayer.redraw();
  //       store_data_busqueda.removeAll();
  //       store_data_export.removeAll();
  //       var layers = map.getLayersByName('Centro');
  //       for (var layerIndex = 0; layerIndex < layers.length; layerIndex++) {
  //         map.removeLayer(layers[layerIndex]);
  //       }
  //       var layers = map.getLayersByName('Busca');
  //       for (var layerIndex = 0; layerIndex < layers.length; layerIndex++) {
  //         map.removeLayer(layers[layerIndex]);
  //       }
  //       polygonWindow.hide();
  //     }
  //   }
  // ]
});

// grid para los resultados del polygon select
var jurisGrid = new Ext.grid.GridPanel({
  store: store_jurisdicciones,
  id: 'jurisGrid',
  tbar : [{
    xtype: 'exportbutton',
    store: store_jurisdicciones,
    exportFunction: 'exportStore',
    text: 'Exportar a Excel',
    title: 'ADIF',
    filename: 'seleccion.xls'
  }],
  // plugins: expander,
  loadMask: {msg:'Obteniendo datos...'},
  enableHdMenu: true,
  viewConfig: {forceFit: true},
  // startCollapsed: true,
  enableColumnResize : true,
  columns: [
    // {header: 'ID', sortable: false, dataIndex: 'id'},
    {header: 'Nombre', sortable: true, dataIndex: 'Nombre'},
    // {header: 'Tipo', sortable: true, dataIndex: 'Tipo'},
    {header: 'FFCC', sortable: true, dataIndex: 'ffcc'}
  ],
  frame: false,
  width: 395,
  height: 480,
  autowidth: true,
  collapsible: false,
  border: false,
  animCollapse: false,
  iconCls: 'icon-grid'
});

// window para el polygon grid
var jurisWindow = new Ext.Window({
  id: 'jurisWindow',
  title: "Selección por jurisdicción",
  width: "75%",
  scripts: true,
  loadScripts: true,
  height: "50%",
  layout: 'fit',
  closable: true,
  closeAction: 'hide',
  constrain: true,
  shadow: false,
  buttonAlign: 'center',
  resizable: true,
  items: [jurisGrid],
  listeners: {
    close: function(){
      console.log('Window closed');
      console.log(arguments);
    }
  }
});

// create feature store, binding it to the vector layer
// var xstore = new GeoExt.data.FeatureStore({
//   fields: [{
//     name: 'nombre',
//     type: 'string'
//   }, {
//     name: 'tipo',
//     type: 'string'
//   }],
//
//
//
//   proxy: new GeoExt.data.ProtocolProxy({
//     protocol: new OpenLayers.Protocol.WFS({
//       url: "http://geoserver.staging.adif.interactar.com:80/geoserver/wfs",
//       featureType: "ffcc_nacional_adif_v3",
//       geometryName: 'the_geom',
//       // featureNS: "http://opengeo.org",
//       srsName: "EPSG:4326",
//       version: "1.1.0",
//       filters: [new OpenLayers.Filter.Spatial({
//         type: OpenLayers.Filter.Spatial.INTERSECTS,
//         value: new OpenLayers.Geometry(new OpenLayers.Geometry.Point(0,0))
//       })]
//     })
//   }),
//   autoLoad: false
// });

// store_exp = new Ext.data.Store({
//   url: '/wfsbufferselect',
//   reader: new Ext.data.JsonReader({
//     root: 'features',
//     // idProperty: 'id',
//     totalProperty: 'totalFeatures',
//     fields: [
//       // {name: 'id'},
//       {name: 'Tipo', mapping: 'geometry.type'},
//       {name: 'Nombre', mapping: 'properties.name'},
//       {name: 'ffcc', mapping: 'properties.nombre'}
//     ]
//   }),
//   storeId: 'myExportStore'
// });
  // url: '/wfsbufferselect'
// store_test = new Ext.data.GroupingStore({
//   url: '/wfsbufferselect',
//   reader: new Ext.data.JsonReader({root:"features"}),
//   groupField: 'type'
// });

//store
store_data_busqueda = new Ext.data.GroupingStore({
  url: 'includes/multigrid.php',
  reader: new Ext.data.JsonReader({
    root: 'rows'
  }, [
    //{name: 'd1', mapping: 'd1'},
    {
      name: 'nombre',
      mapping: 'nombre'
    }, {
      name: 'capa',
      mapping: 'capa'
    }

  ]),
  groupField: 'capa',
  startCollapsed: true
});


// store_data_export = new Ext.data.GroupingStore({
//   url: 'includes/multigrid_export.php',
//   reader: new Ext.data.JsonReader({
//     root: 'rows'
//   }, [
//     //{name: 'd1', mapping: 'd1'},
//     {
//       name: 'nombre',
//       mapping: 'nombre'
//     }, {
//       name: 'capa',
//       mapping: 'capa'
//     }, {
//       name: 'col0',
//       mapping: 'col0'
//     }, {
//       name: 'col1',
//       mapping: 'col1'
//     }, {
//       name: 'col2',
//       mapping: 'col2'
//     }, {
//       name: 'col3',
//       mapping: 'col3'
//     }, {
//       name: 'col4',
//       mapping: 'col4'
//     }, {
//       name: 'col5',
//       mapping: 'col5'
//     }, {
//       name: 'col6',
//       mapping: 'col6'
//     }, {
//       name: 'col7',
//       mapping: 'col7'
//     }, {
//       name: 'col8',
//       mapping: 'col8'
//     }, {
//       name: 'col9',
//       mapping: 'col9'
//     }, {
//       name: 'col10',
//       mapping: 'col10'
//     }, {
//       name: 'col11',
//       mapping: 'col11'
//     }, {
//       name: 'col12',
//       mapping: 'col12'
//     }, {
//       name: 'col13',
//       mapping: 'col13'
//     }, {
//       name: 'col14',
//       mapping: 'col14'
//     }, {
//       name: 'col15',
//       mapping: 'col15'
//     }, {
//       name: 'col16',
//       mapping: 'col16'
//     }, {
//       name: 'col17',
//       mapping: 'col17'
//     }, {
//       name: 'col18',
//       mapping: 'col18'
//     }, {
//       name: 'col19',
//       mapping: 'col19'
//     }, {
//       name: 'col20',
//       mapping: 'col20'
//     }, {
//       name: 'col21',
//       mapping: 'col21'
//     }, {
//       name: 'col22',
//       mapping: 'col22'
//     }, {
//       name: 'col23',
//       mapping: 'col23'
//     }, {
//       name: 'col24',
//       mapping: 'col24'
//     }, {
//       name: 'col25',
//       mapping: 'col25'
//     }

//   ]),
//   groupField: 'capa',
//   startCollapsed: true
// });


//expander
expander = new Ext.ux.grid.RowExpander({
  tpl: new Ext.Template(
    '{col0}',
    '{col1}',
    '{col2}',
    '{col3}',
    '{col4}',
    '{col5}',
    '{col6}',
    '{col7}',
    '{col8}',
    '{col9}',
    '{col10}',
    '{col11}',
    '{col12}',
    '{col13}',
    '{col14}',
    '{col15}',
    '{col16}',
    '{col17}',
    '{col18}',
    '{col19}',
    '{col20}',
    '{col21}',
    '{col22}',
    '{col23}',
    '{col24}',
    '{col25}'
  )
});




//grid
// grid_buscador = new Ext.grid.GridPanel({
//   store: store_data_busqueda,
//   tbar: [{
//     xtype: 'exportbutton',
//     store: store_data_export
//   }],
//   plugins: expander,
//   loadMask: true,
//   enableHdMenu: true,
//   startCollapsed: true,
//   enableColumnResize: true,
//   columns: [
//     expander, {
//       header: "",
//       sortable: true,
//       dataIndex: 'nombre'
//     }, {
//       header: "",
//       sortable: true,
//       dataIndex: 'col2',
//       align: 'left',
//       width: 85
//     }, {
//       header: "capa",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'capa'
//     }, {
//       header: "x",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'x'
//     }, {
//       header: "y",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'y'
//     }, {
//       header: "link",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'link'
//     }, {
//       header: "xy",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'xy'
//     }, {
//       header: "col3",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col3'
//     }, {
//       header: "col4",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col4'
//     }, {
//       header: "col5",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col5'
//     }, {
//       header: "col6",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col6'
//     }, {
//       header: "col7",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col7'
//     }, {
//       header: "col8",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col8'
//     }, {
//       header: "col9",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col9'
//     }, {
//       header: "col10",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col10'
//     }, {
//       header: "col11",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col11'
//     }, {
//       header: "col12",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col12'
//     }, {
//       header: "col13",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col13'
//     }, {
//       header: "col14",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col14'
//     }, {
//       header: "col15",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col15'
//     }, {
//       header: "col16",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col16'
//     }, {
//       header: "col17",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col17'
//     }, {
//       header: "col18",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col18'
//     }, {
//       header: "col19",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col19'
//     }, {
//       header: "col20",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col20'
//     }, {
//       header: "col21",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col21'
//     }, {
//       header: "col22",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col22'
//     }, {
//       header: "col23",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col23'
//     }, {
//       header: "col24",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col24'
//     }, {
//       header: "col25",
//       sortable: true,
//       hidden: true,
//       dataIndex: 'col25'
//     }, {
//       header: '',
//       align: 'center',
//       hidden: true,
//       renderer: verInfo,
//       width: 100
//     }
//   ],
//   view: new Ext.grid.GroupingView({
//     forceFit: true,
//     groupTextTpl: '<div class="regio-level2">{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})</div>'
//   }),
//   frame: false,
//   width: 395,
//   height: 480,
//   autowidth: true,
//   collapsible: false,
//   border: false,
//   animCollapse: false,
//   iconCls: 'icon-grid',
//   listeners: {
//     onRender: function() {

//       // let it render first
//       grid_buscador.superclass.onRender.apply(this, arguments);

//       //do it after the gridView is initialized !!
//       //this.view.refresh();
//       this.loadMask.show();

//       // defer the store.load so browser can catch up
//       this.store_data_busqueda.load();

//     }
//   }
// });





function verInfo(value, id, r) {}

function createGridButton(value, id, record) {
  new Ext.Button({
    text: value,
    handler: function(btn, e) {}
  }).render(document.body, id);
}



//rowclick
// grid_buscador.on('rowclick', function(grid, rowIndex, e) {


//   var layers = map.getLayersByName('Busca');
//   for (var layerIndex = 0; layerIndex < layers.length; layerIndex++) {
//     map.removeLayer(layers[layerIndex]);
//   }
//   var regsel = grid_buscador.getSelectionModel().getSelected();
//   var coordgeo = new OpenLayers.LonLat(regsel.get('x'), regsel.get('y'));
//   var coordgeo = new OpenLayers.LonLat(regsel.get('x'), regsel.get('y'));
//   var coorddestino = coordgeo.transform(new OpenLayers.Projection('EPSG:4326'), new OpenLayers.Projection('EPSG:1991'));
//   map.panTo(coorddestino);
//   var wkt = new OpenLayers.Format.WKT();
//   var str2 = regsel.get('xy');
//   features = wkt.read(str2);
//   var style1 = {
//     fillColor: '#FFFF33',
//     fillOpacity: 0.8
//   };
//   var styleMap = new OpenLayers.StyleMap(OpenLayers.Util.applyDefaults({
//     fillColor: "#EAC680",
//     fillOpacity: 0.3,
//     strokeColor: "#EB9F15",
//     strokeWidth: 3,
//     label: "",
//     fontSize: "9px"
//   }, OpenLayers.Feature.Vector.style["default"]));
//   var thrvec = new OpenLayers.Layer.Vector('Busca', {
//     styleMap: styleMap
//   });
//   map.addLayers([thrvec]);
//   thrvec.addFeatures(features);

// });



// store_data_busqueda.on('load', function() {
//   var layers = map.getLayersByName('Busca');
//   for (var layerIndex = 0; layerIndex < layers.length; layerIndex++) {
//     map.removeLayer(layers[layerIndex]);
//   }
// });
