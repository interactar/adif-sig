/*  ---CONFIGS---  (GeoExt.tree.LayerContainer --> Ext.tree.AsyncTreeNode)
allowChildren
allowDrag
allowDrop
checked
cls
disabled
draggable
editable
expandable
expanded
hidden
href
hrefTarget
icon
iconCls
id
isTarget
leaf
listeners
loader
qtip
qtipCfg
singleClickExpand
text
uiProvider
*/

var tree_groups = {

  "default": {
    title: "Capas adicionales",
    expanded: false,
    checked: true
  },
  "estaciones": {
    title: "Estaciones",
    expanded: false,
    checked: true
  },
  "indec": {
    title: "Localidades",
    expanded: false
  },
  "tipo": {
    title: "Tipo de servicio",
    expanded: false
  },
  "estado": {
    title: "Estado de servicio",
    expanded: false
  },
  "obras_tipo": {
    title: "Obras por Tipo",
    expanded: true
  },
  "obras_estado": {
    title: "Obras por Estado",
    expanded: false
  },
  "ffcc": {
    title: "FFCC",
    expanded: false,
    checked: true
  },
  "background": {
    title: "Mapa base",
    expanded: false,
    exclusive: true
  }
};

Ext.iterate(tree_groups, function(key, value){
  if(value.exclusive) return;
  value.checked = value.checked || false;
  value.isLeaf = false;
  value.listeners = {
    checkchange: function(layerTree, newValue){
      // console.log(layerTree.childrenRendered);
      // if(!layerTree.childrenRendered) layerTree.renderChildren();

      if(!layerTree.expanded && layerTree.childNodes.length == 0) layerTree.expand();
      layerTree.eachChild(function(node){
        node.ui.toggleCheck(newValue);
      });
    }
  };
});